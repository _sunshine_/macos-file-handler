//
//  MainViewController.m
//  File Handler
//
//  Created by Maksym Shulha on 12.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import "MainViewController.h"
#import "UserFileViewModel.h"
#import "Consts.h"

@interface MainViewController () <NSTableViewDataSource, NSTableViewDelegate>

@property (weak, nonatomic) IBOutlet NSTableView *tableView;
@property (weak, nonatomic) IBOutlet NSButton *addFileButton;
@property (weak, nonatomic) IBOutlet NSButton *removeFileButton;
@property (weak, nonatomic) IBOutlet NSButton *runButton;
@property (weak, nonatomic) IBOutlet NSPopUpButton *popUpButton;
@property (weak, nonatomic) IBOutlet NSProgressIndicator *progressIndicator;
@property (weak, nonatomic) IBOutlet NSTextView *textView;

@property (nonatomic) NSArray<UserFileViewModel *> *itemList;

@end

@implementation MainViewController

#pragma mark - NSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.presenter viewDidLoad];
}

#pragma mark - NSTableViewDataSource

- (NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView {
    return self.itemList.count;
}

- (NSView *)tableView:(NSTableView *)aTableView
   viewForTableColumn:(NSTableColumn *)aTableColumn
                  row:(NSInteger)aRow {
    NSString *columnIdentifier = aTableColumn.identifier;
    NSTableCellView *cell = [aTableView makeViewWithIdentifier:columnIdentifier
                                                         owner:nil];
    
    if (cell) {
        UserFileViewModel *model = [self.itemList objectAtIndex:aRow];
        NSString *value = nil;
        if ([columnIdentifier isEqualToString:Defaults.View.TableView.Column1Identifier]) {
            value = model.name;
        }
        else if ([columnIdentifier isEqualToString:Defaults.View.TableView.Column2Identifier]) {
            value = model.path;
        }
        cell.textField.stringValue = value;
    }
    
    return cell;
}

- (void)tableViewSelectionDidChange:(NSNotification *)aNotification {
    [self.presenter tableViewDidChangeSelection:self.tableView.selectedRowIndexes];
}

#pragma mark - MainViewInput

- (void)setUserFileList:(NSArray<UserFileViewModel *> *)aList {
    self.itemList = aList;
    [self.tableView reloadData];
}

- (void)setAddFileButtonEnabled:(BOOL)isEnabled {
    [self.addFileButton setEnabled:isEnabled];
}

- (void)setRemoveFileButtonEnabled:(BOOL)isEnabled {
    [self.removeFileButton setEnabled:isEnabled];
}

- (void)setRunButtonEnabled:(BOOL)isEnabled {
    [self.runButton setEnabled:isEnabled];
}

- (void)presentOpenPanelWithOKHandler:(void (^)(NSArray<NSURL *> *))aHandler {
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    openPanel.allowsMultipleSelection = true;
    
    [openPanel beginWithCompletionHandler:^(NSModalResponse result) {
        switch (result) {
            case NSModalResponseOK: {
                aHandler(openPanel.URLs);
                break;
            }
            default:
                break;
        }
        [self.presenter openPanelDidClosed];
    }];
}

- (void)setProgress:(double)aValue {
    [self.progressIndicator setDoubleValue:aValue];
}

- (void)cleanTextView {
    [self.textView setString:@""];
}

- (void)appendTextToTextView:(NSString *)aText {
    NSString *oldString = self.textView.string;
    NSString *newString = [NSString stringWithFormat:@"%@%@%@", oldString, (oldString.length == 0 ? @"" : @"\n\n"), aText];
    [self.textView setString:newString];
}

- (void)setPopUpButtonTitleList:(NSArray<NSString *> *)aTitleList {
    [self.popUpButton removeAllItems];
    [self.popUpButton addItemsWithTitles:aTitleList];
}

#pragma mark - IBAction

- (IBAction)addFileButton_TouchUpInside:(NSButton *)aSender {
    [self.presenter addFileButton_TouchUpInside];
}

- (IBAction)removeFileButton_TouchUpInside:(NSButton *)aSender {
    [self.presenter removeFileButton_TouchUpInside];
}

- (IBAction)submitButton_TouchUpInside:(NSButton *)aSender {
    [self.presenter runButton_TouchUpInside];
}

- (IBAction)popUpButton_ValueDidChanged:(NSPopUpButton *)aSender {
    [self.presenter popUpButton_ValueChanged:aSender.indexOfSelectedItem];
}

@end
