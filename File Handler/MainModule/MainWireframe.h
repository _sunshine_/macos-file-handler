//
//  MainRouter.h
//  File Handler
//
//  Created by Maksym Shulha on 17.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WireframeProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface MainWireframe : NSObject <WireframeProtocol>

@end

NS_ASSUME_NONNULL_END
