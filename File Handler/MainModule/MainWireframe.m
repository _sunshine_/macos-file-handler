//
//  MainRouter.m
//  File Handler
//
//  Created by Maksym Shulha on 17.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import "MainWireframe.h"
#import "MainPresenter.h"
#import "MainViewController.h"
#import <Cocoa/Cocoa.h>
#import "DataStore.h"
#import "FileService.h"

@implementation MainWireframe

#pragma mark - WireframeProtocol

+ (NSViewController *)createModule {
    NSViewController *viewController = [[NSStoryboard storyboardWithName:NSStringFromClass([MainViewController class])
                                                                  bundle:nil] instantiateInitialController];
    if (viewController &&
        [viewController isMemberOfClass:[MainViewController class]] &&
        [viewController conformsToProtocol:@protocol(MainViewInput)]) {
        MainViewController *view = (MainViewController *)viewController;
        MainPresenter *presenter = [[MainPresenter alloc] initWithView:view];
        presenter.dataStore = DataStore.shared;
        presenter.fileService = FileService.shared;
        view.presenter = presenter;
    }
    
    return viewController;
}

@end
