//
//  MainPresenter.m
//  File Handler
//
//  Created by Maksym Shulha on 17.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import "MainPresenter.h"
#import "UserFile.h"
#import "UserFileViewModel.h"
#import "Enums.h"

@interface MainPresenter ()

@property (nonatomic, weak) id<MainViewInput> view;
@property (nonatomic) NSArray<UserFile *> *itemList;
@property (nonatomic, readonly) NSArray<UserFile *> *selectedItemList;
@property (nonatomic) NSIndexSet *selectedIndexes;
@property (nonatomic) FileHandlingAction action;
@property (nonatomic, getter=isPerformingOperation) BOOL performingOperation;
@property (nonatomic) NSInteger totalFileCount;
@property (atomic) NSNumber *handledFileCount;

@end

@implementation MainPresenter

#pragma mark - Accessors

- (void)setItemList:(NSArray<UserFile *> *)anItemList {
    _itemList = anItemList;
    
    NSMutableArray *viewModelList = [NSMutableArray array];
    
    for (UserFile *item in anItemList) {
        UserFileViewModel *model = [[UserFileViewModel alloc] initWithUserFile:item];
        if (model) {
            [viewModelList addObject:model];
        }
    }
    
    [self.view setUserFileList:viewModelList];
}

- (NSArray<UserFile *> *)selectedItemList {
    NSMutableArray *result = [NSMutableArray array];
    
    [self.selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL * _Nonnull stop) {
        if (NSLocationInRange(idx, NSMakeRange(0, self.itemList.count))) {
            [result addObject:[self.itemList objectAtIndex:idx]];
        }
    }];
    
    return result;
}

- (void)setPerformingOperation:(BOOL)isPerformingOperation {
    _performingOperation = isPerformingOperation;
    
    BOOL hasSelectedFiles = (self.selectedIndexes.count > 0);
    [self.view setAddFileButtonEnabled:!isPerformingOperation];
    [self.view setRemoveFileButtonEnabled:(!isPerformingOperation && hasSelectedFiles)];
    [self.view setRunButtonEnabled:(!isPerformingOperation && hasSelectedFiles)];
}

#pragma mark - Public

- (instancetype)initWithView:(id<MainViewInput>)aView {
    self = [super init];
    
    if (self) {
        _view = aView;
    }
    
    return self;
}

#pragma mark - MainViewOutput

- (void)viewDidLoad {
    self.action = kReadBinaryData;
    [self.view setPopUpButtonTitleList:[FileHandlingActionEnum allFileHandlingActionTitles]];
    self.itemList = [self.dataStore getUserFiles];
}

- (void)tableViewDidChangeSelection:(NSIndexSet *)anIndexSet {
    self.selectedIndexes = anIndexSet;
    BOOL isEnabled = (!self.isPerformingOperation && anIndexSet.count > 0);
    [self.view setRemoveFileButtonEnabled:isEnabled];
    [self.view setRunButtonEnabled:isEnabled];
}

- (void)addFileButton_TouchUpInside {
    self.performingOperation = true;
    [self.view presentOpenPanelWithOKHandler:^(NSArray<NSURL *> *anURLs) {
        for (NSURL *url in anURLs) {
            NSError *error;
            NSData *bookmarkData = [url bookmarkDataWithOptions:NSURLBookmarkCreationWithSecurityScope
                                 includingResourceValuesForKeys:nil
                                                  relativeToURL:nil
                                                          error:&error];
            if (!error &&
                bookmarkData) {
                [self.dataStore createUserFile:url.absoluteString
                                  bookmarkData:bookmarkData];
            }
        }
        self.itemList = [self.dataStore getUserFiles];
    }];
}

- (void)removeFileButton_TouchUpInside {
    self.performingOperation = true;
    for (UserFile *file in self.selectedItemList) {
        [self.dataStore deleteUserFile:file];
    }
    self.itemList = [self.dataStore getUserFiles];
    self.selectedIndexes = [[NSIndexSet alloc] init];
    self.performingOperation = false;
}

- (void)runButton_TouchUpInside {
    NSArray *selectedItemList = self.selectedItemList;
    NSInteger selectedItemCount = selectedItemList.count;
    if (selectedItemCount > 0) {
        self.performingOperation = true;
        [self.view setProgress:0];
        [self.view cleanTextView];
        
        self.totalFileCount = selectedItemCount;
        self.handledFileCount = @(0);
        
        for (UserFile *file in selectedItemList) {
            NSURL *url = [NSURL URLByResolvingBookmarkData:file.bookmarkData
                                                   options:NSURLBookmarkResolutionWithSecurityScope
                                             relativeToURL:nil
                                       bookmarkDataIsStale:nil
                                                     error:nil];
            [url startAccessingSecurityScopedResource];
            
            void (^block)(NSURL *) = ^void(NSURL *anURL) {
                @synchronized (self.handledFileCount) {
                    self.handledFileCount = @(self.handledFileCount.integerValue + 1);
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    @synchronized (self.handledFileCount) {
                        if (self.totalFileCount == self.handledFileCount.integerValue) {
                            self.performingOperation = false;
                        }
                        [self.view setProgress:(double)self.handledFileCount.integerValue/(double)self.totalFileCount * 100];
                    }
                });
                [anURL stopAccessingSecurityScopedResource];
            };
            switch (self.action) {
                case kReadBinaryData: {
                    NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingAtPath:url.path];
                    [self.fileService getDataFileHandle:fileHandle
                                              withReply:^(NSData * _Nonnull aData) {
                                                  [fileHandle closeFile];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [self.view appendTextToTextView:[NSString stringWithFormat:@"File: %@\nData: %@", url.path, aData]];
                                                  });
                                                  block(url);
                                              }];
                    break;
                }
                case kGetFileSize: {
                    [self.fileService getFileSizeFromURL:url
                                               withReply:^(unsigned long long aFileSize) {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [self.view appendTextToTextView:[NSString stringWithFormat:@"File: %@\nSize: %lld", url.path, aFileSize]];
                                                   });
                                                   block(url);
                                               }];
                    break;
                }
            }
        }
    }
}

- (void)popUpButton_ValueChanged:(NSInteger)anIndex {
    self.action = (FileHandlingAction)anIndex;
}

- (void)openPanelDidClosed {
    self.performingOperation = false;
}

@end
