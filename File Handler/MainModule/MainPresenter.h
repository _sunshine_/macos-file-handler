//
//  MainPresenter.h
//  File Handler
//
//  Created by Maksym Shulha on 17.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MainViewProtocol.h"
#import "UserFileDataStoreProtocol.h"
#import "FileHandlerProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface MainPresenter : NSObject <MainViewOutput>

@property (nonatomic) id<UserFileDataStoreProtocol> dataStore;
@property (nonatomic) id<FileHandlerProtocol> fileService;

- (instancetype)initWithView:(id<MainViewInput>)aView;

@end

NS_ASSUME_NONNULL_END
