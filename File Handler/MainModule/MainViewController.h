//
//  MainViewController.h
//  File Handler
//
//  Created by Maksym Shulha on 12.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "MainViewProtocol.h"
#import "UserFileDataStoreProtocol.h"
#import "FileHandlerProtocol.h"

@interface MainViewController : NSViewController <MainViewInput>

@property (nonatomic) id<MainViewOutput> presenter;
@property (nonatomic) id<UserFileDataStoreProtocol> dataStore;
@property (nonatomic) id<FileHandlerProtocol> fileService;

@end
