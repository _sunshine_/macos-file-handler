//
//  Consts.m
//  File Handler
//
//  Created by Maksym Shulha on 14.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import "Consts.h"

const DefaultsStruct Defaults = {
    .CoreData = {
        .StorePath = @"CoreData.sqlite",
        .ModelFile = {
            .FileName = @"CoreDataModels",
            .FileExtension = @"momd"
        },
        .Model = {
            .UserFile = @"UserFile"
        }
    },
    .XPCConnection = {
        .ServiceName = @"com.macpaw.test.FileService"
    },
    .View = {
        .TableView = {
            .Column1Identifier = @"column1",
            .Column2Identifier = @"column2"
        }
    }
};
