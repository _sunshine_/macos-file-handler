//
//  Consts.h
//  File Handler
//
//  Created by Maksym Shulha on 14.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef struct {
    struct CoreDataStruct {
        NSString * const StorePath;
        struct ModelFileStruct {
            NSString * const FileName;
            NSString * const FileExtension;
        } ModelFile;
        struct ModelStruct {
            NSString * const UserFile;
        } Model;
    } CoreData;
    struct XPCConnectionStruct {
        NSString * const ServiceName;
    } XPCConnection;
    struct ViewStruct {
        struct TableViewStruct {
            NSString * const Column1Identifier;
            NSString * const Column2Identifier;
        } TableView;
    } View;
} DefaultsStruct;

extern const DefaultsStruct Defaults;
