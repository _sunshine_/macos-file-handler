//
//  Enums.m
//  File Handler
//
//  Created by Maksym Shulha on 14.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import "Enums.h"

@implementation FileHandlingActionEnum

#pragma mark - FileHandlingActionEnum (Public)

+ (NSArray<NSNumber *> *)allFileHandlingActions {
    static NSArray<NSNumber *> *actions;
    static dispatch_once_t actionsOnceToken;
    
    dispatch_once(&actionsOnceToken, ^{
        actions = @[@(kReadBinaryData),
                    @(kGetFileSize)];
    });
    
    return actions;
}

+ (NSArray<NSString *> *)allFileHandlingActionTitles {
    static NSArray<NSString *> *actionTitles;
    static dispatch_once_t actionTitlesOnceToken;
    
    dispatch_once(&actionTitlesOnceToken, ^{
        NSMutableArray *titles = [NSMutableArray array];
        for (NSNumber *action in [[self class] allFileHandlingActions]) {
            NSString *actionTitle = [[self class] fileHandlingActionToString:(FileHandlingAction)action.integerValue];
            if (actionTitle) {
                [titles addObject:actionTitle];
            }
            else {
                [titles addObject:[NSString stringWithFormat:@"Action %d", action.intValue]];
            }
        }
        
        actionTitles = titles;
    });
    
    return actionTitles;
}

#pragma mark - FileHandlingActionEnum (Private)

+ (NSString *)fileHandlingActionToString:(FileHandlingAction)anAction {
    NSString *result = nil;
    
    switch (anAction) {
        case kReadBinaryData:
            result = @"Read binary data";
            break;
        case kGetFileSize:
            result = @"Get file size";
            break;
    }
    
    return result;
}

@end
