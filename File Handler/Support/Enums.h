//
//  Enums.h
//  File Handler
//
//  Created by Maksym Shulha on 14.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, FileHandlingAction) {
    kReadBinaryData,
    kGetFileSize
};

@interface FileHandlingActionEnum : NSObject

+ (NSArray<NSNumber *> *)allFileHandlingActions;
+ (NSArray<NSString *> *)allFileHandlingActionTitles;

@end

NS_ASSUME_NONNULL_END
