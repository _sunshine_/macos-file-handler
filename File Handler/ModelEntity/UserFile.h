//
//  UserFile.h
//  File Handler
//
//  Created by Maksym Shulha on 12.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserFile : NSManagedObject

@property (nonatomic) NSString *stringURL;
@property (nonatomic) NSData *bookmarkData;

@end

NS_ASSUME_NONNULL_END
