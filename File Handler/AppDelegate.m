//
//  AppDelegate.m
//  File Handler
//
//  Created by Maksym Shulha on 12.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import "AppDelegate.h"
#import "MainWireframe.h"
#import "DataStore.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    [NSApplication.sharedApplication.mainWindow setContentViewController:[MainWireframe createModule]];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    [DataStore.shared saveContext];
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
    return NSTerminateNow;
}

@end
