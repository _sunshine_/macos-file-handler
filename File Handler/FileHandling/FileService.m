//
//  FileService.m
//  File Handler
//
//  Created by Maksym Shulha on 13.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import "FileService.h"
#import <ApplicationServices/ApplicationServices.h>
#import "Consts.h"

@interface FileService ()

@property (nonatomic) NSXPCConnection *connection;

@end

@implementation FileService

#pragma mark - NSObject

- (void)dealloc {
    [self.connection invalidate];
}

#pragma mark - Accessors

+ (FileService *)shared {
    static FileService *sharedInstance = nil;
    static dispatch_once_t sharedInstanceOnceToken;
    
    dispatch_once(&sharedInstanceOnceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (NSXPCConnection *)connection {
    if (_connection == nil) {
        _connection = [[NSXPCConnection alloc] initWithServiceName:Defaults.XPCConnection.ServiceName];
        _connection.remoteObjectInterface = [NSXPCInterface interfaceWithProtocol:@protocol(FileHandlerProtocol)];
        [_connection resume];
    }
    
    return _connection;
}

#pragma mark - FileDataSource

- (void)getDataFileHandle:(NSFileHandle *)aHandle
                withReply:(void (^)(NSData *))aReply {
    [self.connection.remoteObjectProxy getDataFileHandle:aHandle
                                               withReply:aReply];
}

- (void)getFileSizeFromURL:(NSURL *)anURL
                 withReply:(void (^)(unsigned long long))aReply {
    [self.connection.remoteObjectProxy getFileSizeFromURL:anURL
                                                withReply:aReply];
}

@end
