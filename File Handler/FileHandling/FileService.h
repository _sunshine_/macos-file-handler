//
//  FileService.h
//  File Handler
//
//  Created by Maksym Shulha on 13.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileHandlerProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface FileService : NSObject <FileHandlerProtocol>

@property (class, readonly) FileService *shared;

@end

NS_ASSUME_NONNULL_END
