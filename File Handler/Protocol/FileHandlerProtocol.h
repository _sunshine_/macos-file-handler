//
//  FileHandlerProtocol.h
//  File Handler
//
//  Created by Maksym Shulha on 14.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

@protocol FileHandlerProtocol <NSObject>

- (void)getDataFileHandle:(NSFileHandle *)aHandle
                withReply:(void (^)(NSData *))aReply;
- (void)getFileSizeFromURL:(NSURL *)anURL
                 withReply:(void (^)(unsigned long long))aReply;

@end
