//
//  MainViewProtocol.h
//  File Handler
//
//  Created by Maksym Shulha on 17.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

@class UserFileViewModel;

@protocol MainViewInput <NSObject>

- (void)setUserFileList:(NSArray<UserFileViewModel *> *)aList;
- (void)setAddFileButtonEnabled:(BOOL)isEnabled;
- (void)setRemoveFileButtonEnabled:(BOOL)isEnabled;
- (void)setRunButtonEnabled:(BOOL)isEnabled;
- (void)presentOpenPanelWithOKHandler:(void (^)(NSArray<NSURL *> *))aHandler;
- (void)setProgress:(double)aValue;
- (void)cleanTextView;
- (void)appendTextToTextView:(NSString *)aText;
- (void)setPopUpButtonTitleList:(NSArray<NSString *> *)aTitleList;

@end

@protocol MainViewOutput <NSObject>

- (void)viewDidLoad;
- (void)tableViewDidChangeSelection:(NSIndexSet *)anIndexSet;
- (void)addFileButton_TouchUpInside;
- (void)removeFileButton_TouchUpInside;
- (void)runButton_TouchUpInside;
- (void)popUpButton_ValueChanged:(NSInteger)anIndex;
- (void)openPanelDidClosed;

@end
