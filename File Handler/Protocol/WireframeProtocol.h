//
//  WireframeProtocol.h
//  File Handler
//
//  Created by Maksym Shulha on 17.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

@class NSViewController;

@protocol WireframeProtocol <NSObject>

+ (NSViewController *)createModule;

@end
