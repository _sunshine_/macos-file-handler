//
//  UserFileDataStoreProtocol.h
//  File Handler
//
//  Created by Maksym Shulha on 14.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

@class UserFile;

@protocol UserFileDataStoreProtocol <NSObject>

- (NSArray<UserFile *> *)getUserFiles;
- (void)createUserFile:(NSString *)aStringURL
          bookmarkData:(NSData *)aBookmarkData;
- (void)deleteUserFile:(UserFile *)aUserFile;

@end

