//
//  DataStore.h
//  File Handler
//
//  Created by Maksym Shulha on 12.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserFileDataStoreProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface DataStore : NSObject <UserFileDataStoreProtocol>

@property (class, readonly) DataStore *shared;

- (BOOL)saveContext;

@end

NS_ASSUME_NONNULL_END
