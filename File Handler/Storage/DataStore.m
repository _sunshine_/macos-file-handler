//
//  DataStore.m
//  File Handler
//
//  Created by Maksym Shulha on 12.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import "DataStore.h"
#import <CoreData/CoreData.h>
#import "UserFile.h"
#import "Consts.h"

@interface DataStore ()

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation DataStore

#pragma mark - Accessors

+ (DataStore *)shared {
    static DataStore *sharedInstance = nil;
    static dispatch_once_t sharedInstanceOnceToken;
    
    dispatch_once(&sharedInstanceOnceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel == nil) {
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:Defaults.CoreData.ModelFile.FileName
                                                  withExtension:Defaults.CoreData.ModelFile.FileExtension];
        _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    }
    
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator == nil) {
        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        
        NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:Defaults.CoreData.StorePath];
        [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                  configuration:nil
                                                            URL:storeURL
                                                        options:nil
                                                          error:nil];
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext == nil) {
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        if (coordinator != nil) {
            _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
            [_managedObjectContext setPersistentStoreCoordinator:coordinator];
        }
    }
    
    return _managedObjectContext;
}

#pragma mark - DataSource (Public)

- (BOOL)saveContext {
    BOOL result = false;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    
    if (managedObjectContext != nil &&
        (![managedObjectContext hasChanges] ||
         [managedObjectContext save:nil])) {
            result = true;
        }
    
    return result;
}

#pragma mark - DataSource (Private)

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - UserFileDataSource

- (void)createUserFile:(NSString *)stringURL
          bookmarkData:(NSData *)bookmarkData {
    UserFile *userFile = [NSEntityDescription insertNewObjectForEntityForName:Defaults.CoreData.Model.UserFile
                                                       inManagedObjectContext:[self managedObjectContext]];
    userFile.stringURL = stringURL;
    userFile.bookmarkData = bookmarkData;
    
    [self saveContext];
}

- (void)deleteUserFile:(UserFile *)userFile {
    [[self managedObjectContext] deleteObject:userFile];
    [self saveContext];
}

- (NSArray *)getUserFiles {
    NSManagedObjectContext *moc = [self managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:Defaults.CoreData.Model.UserFile];
    NSArray *results = [moc executeFetchRequest:request
                                          error:nil];
    
    return results;
}

@end
