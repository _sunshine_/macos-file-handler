//
//  UserFileViewModel.m
//  File Handler
//
//  Created by Maksym Shulha on 17.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import "UserFileViewModel.h"
#import "UserFile.h"

@implementation UserFileViewModel

#pragma mark - NSObject

- (instancetype)init {
    return [self initWithName:@""
                         path:@""];
}

#pragma mark - Public

- (instancetype)initWithName:(NSString *)aName
                        path:(NSString *)aPath {
    self = [super init];
    
    if (self) {
        _name = aName;
        _path = aPath;
    }
    
    return self;
}

- (instancetype)initWithUserFile:(UserFile *)aUserFile {
    NSURL *url = [NSURL URLWithString:aUserFile.stringURL];
    return [self initWithName:url.lastPathComponent
                         path:url.URLByDeletingLastPathComponent.path];
}

@end
