//
//  UserFileViewModel.h
//  File Handler
//
//  Created by Maksym Shulha on 17.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserFile;

NS_ASSUME_NONNULL_BEGIN

@interface UserFileViewModel : NSObject

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *path;

- (instancetype)initWithName:(NSString *)aName
                        path:(NSString *)aPath NS_DESIGNATED_INITIALIZER;
- (instancetype)initWithUserFile:(UserFile *)aUserFile;

@end

NS_ASSUME_NONNULL_END
