//
//  FileServiceProtocol.h
//  FileService
//
//  Created by Maksym Shulha on 13.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FileServiceProtocol

- (void)getDataFileHandle:(NSFileHandle *)aHandle
                withReply:(void (^)(NSData *))aReply;
- (void)getFileSizeFromURL:(NSURL *)anURL
                 withReply:(void (^)(unsigned long long))aReply;

@end
