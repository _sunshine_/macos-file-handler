//
//  main.m
//  FileService
//
//  Created by Maksym Shulha on 13.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileService.h"

@interface ServiceDelegate : NSObject <NSXPCListenerDelegate>
@end

@implementation ServiceDelegate

- (BOOL)listener:(NSXPCListener *)aListener
shouldAcceptNewConnection:(NSXPCConnection *)aNewConnection {
    aNewConnection.exportedInterface = [NSXPCInterface interfaceWithProtocol:@protocol(FileServiceProtocol)];
    FileService *exportedObject = [[FileService alloc] init];
    aNewConnection.exportedObject = exportedObject;
    [aNewConnection resume];
    
    return YES;
}

@end

int main(int argc, const char *argv[]) {
    ServiceDelegate *delegate = [[ServiceDelegate alloc] init];
    NSXPCListener *listener = [NSXPCListener serviceListener];
    listener.delegate = delegate;
    [listener resume];
    
    return 0;
}
