//
//  FileService.m
//  FileService
//
//  Created by Maksym Shulha on 13.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import "FileService.h"

@implementation FileService

#pragma mark - FileServiceProtocol

- (void)getDataFileHandle:(NSFileHandle *)aHandle
                withReply:(void (^)(NSData *))aReply {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        aReply([aHandle readDataToEndOfFile]);
    });
}

- (void)getFileSizeFromURL:(NSURL *)anURL
                 withReply:(void (^)(unsigned long long))aReply {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        aReply([[NSFileManager defaultManager] attributesOfItemAtPath:anURL.path
                                                                error:nil].fileSize);
    });
}

@end
