//
//  FileService.h
//  FileService
//
//  Created by Maksym Shulha on 13.02.2019.
//  Copyright © 2019 Maksym Shulha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileServiceProtocol.h"

@interface FileService : NSObject <FileServiceProtocol>

@end
